import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { StackNavigation } from './src/navigations/StackNavigation';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux'
import {store, persistor} from './src/store/configurestore'
import { View } from 'react-native';
import SplashScreen from 'react-native-splash-screen'
import Home from './src/screens/Home';

export default class App extends React.Component {

  componentDidMount(){
    setTimeout(() => {
      SplashScreen.hide()
    }, 5000);
  }
  render() {
    return (
      <Provider store = {store}>
        <PersistGate persistor = {persistor}>
          <NavigationContainer>
            <Home />
          </NavigationContainer>
        </PersistGate>
      </Provider>
    );
  }
}