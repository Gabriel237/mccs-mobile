import React, { PureComponent } from 'react';
import {  View, Text, StatusBar, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { PrimaryColor } from '@styles'
import { register } from '../../store/actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { styles } from './style'
import FormGroup from '../../components/FormGroup';
import Loading from '../../components/Loading';
import { SET_MESSAGE_ERROR } from '../../store/type'
import RequestError from '../../components/RequestError';

class SignUp extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  handleChange = (value, label) => {
    if(label == 'Nom') {
      this.setState({first_name: value})
    }
    else if(label == 'Prenom') {
      this.setState({last_name: value})
    }
    else if(label == 'Telephone') {
      this.setState({telephone: value})
    }
    else if(label == 'Mot de passe') {
      this.setState({password: value})
    }
    else if(label == 'Username') {
      this.setState({username: value})
    }
  }

  toggleModal = () => {
    const { dispatch } = this.props
    dispatch({type: SET_MESSAGE_ERROR, value: ''})

  }

  register = () => {
    const { register, navigation } = this.props
    const {first_name, last_name, email, password, telephone, username } = this.state
    const data = {first_name, last_name, email, password, telephone, username}
    register(data, navigation)
  }

  render() {
    const { isLoading, messageError } = this.props
    return (
      <ImageBackground 
        source = {require('@assets/42644-m.jpg')} 
        style = {styles.container}
      >
        <StatusBar backgroundColor = {PrimaryColor}/>
        <View style = {styles.header}>
          <Image 
            source = {require('@assets/logo1.png')}
            style = {styles.logo}
          />
        </View>
        <ScrollView 
          style = {styles.content}
          contentContainerStyle = {{paddingBottom: 40}}
          keyboardShouldPersistTaps = 'always'
        >
            <Text style = {styles.title}>Inscription</Text>
            <Loading 
              isVisible = {isLoading}
            />
            {
                messageError != ''?
                <RequestError
                    toggleModal = {this.toggleModal}
                    messageError = {messageError}
                />:null
            }
            <FormGroup 
              label = "Nom"
              iconName = "person"
              onChangeText = {this.handleChange}
            />
            <FormGroup 
              label = "Prenom"
              iconName = "person"
              onChangeText = {this.handleChange}
            />
            <FormGroup 
              label = "Username"
              iconName = "person"
              onChangeText = {this.handleChange}
            />
            <FormGroup 
              label = "Telephone"
              iconName = "phone"
              onChangeText = {this.handleChange}
            />
            <FormGroup 
              label = "Mot de passe"
              iconName = "vpn-key"
              onChangeText = {this.handleChange}
            />
            <TouchableOpacity 
              style = {styles.btn} 
              onPress = {() => this.register()}>
              <Text style = {styles.textBtn}>Suivant</Text>
            </TouchableOpacity>
        </ScrollView>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.authReducer.isLoading,
    messageError: state.bookingReducer.messageError
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    ...bindActionCreators({register}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
