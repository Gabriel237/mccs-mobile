import React, { PureComponent } from 'react';
import {  View, Text, ScrollView, FlatList, TouchableOpacity, ImageBackground, Image } from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Icon } from 'react-native-elements'

import { styles } from './style'
import { listCulte, signIn } from '../../store/actions'
import { capitalize } from '../../utils/capitalize'
import Loading from '../../components/Loading';

class ListCulte extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      MessageInfo: ''
    };
  }

  componentDidMount() {
      this.loadData()
  }

  loadData() {
    const { listCulte,signIn, memoryUser, cultes } = this.props
    signIn(memoryUser).then((res) => {
      if(res.access){
        listCulte(res.access)
        if(cultes !== null && cultes.length == 0){
            this.setState({
              MessageInfo: 'Aucun culte programmé pour le moment'
            })
        }
      }
    })
  }

  renderItem = ({item, index}) => {
      const { navigation, user } = this.props
      console.log('user', user)
      return (
        <TouchableOpacity 
            style = {styles.card}
            onPress = {() => navigation.navigate('Reservation', {culte: item})}
        >
            <View>
              <Image 
                source = {{uri: item.url_picture}}
                style = {styles.imageTheme}
              />
            </View>
            <View style = {{flex: 1}}>
              <Text style = {styles.titleCulte}>{capitalize(item.nomCulte)}</Text>
              <Text style = {styles.subTitleCulte}>{item.description}</Text>
              <View style = {styles.seances}>
                <Icon
                      name = "schedule"
                      type = "material"
                      size = {14}
                />
                <Text>{item.seances.length}</Text>
              </View>
            </View>
            <View style = {styles.contentIcon}>
              <Icon
                name = "navigate-next"
                type = "material"
                size = {25}
                iconStyle = {styles.iconDirection}
              />
            </View>
        </TouchableOpacity>
      )
  }

  render() {
    const { cultes, isLoading } = this.props
    return (
        <ImageBackground 
          source = {require('@assets/42644-m.jpg')}
          style = {styles.container}
          imageStyle = {{top: -250}}
        >
          <View style = {styles.header}>
            <Text style = {styles.headerTitle}>Liste des cultes</Text>
          </View>
            <View 
                style = {styles.content}
            >
              <Loading 
                isVisible = {isLoading}
              />
              {
                cultes !== null && cultes.length != 0?
                <FlatList 
                  data = {cultes}
                  renderItem = {this.renderItem}
                />:
                <Text style = {styles.textInfo}>{this.state.MessageInfo}</Text>
              }
            </View>
        </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      isLoading: state.authReducer.isLoading,
      user: state.authReducer.user,
      memoryUser: state.authReducer.memoryUser,
      cultes: state.culteReducer.list,
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({listCulte, signIn}, dispatch)
  }

export default connect(mapStateToProps, mapDispatchToProps)(ListCulte);
