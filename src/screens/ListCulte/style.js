import { StyleSheet } from 'react-native'
import { PrimaryColor } from '@styles'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: PrimaryColor
    },
    header: {
        height: 100,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 22,
        color: '#fff'
    },
    card: {
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: '#000',
        flexDirection: 'row',
        shadowOffset: {height: 1, width: 1},
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 3,
        marginHorizontal: 2,
        marginVertical: 10,
        padding: 10
    },
    content: {
        flex: 1,
        backgroundColor: '#fff',
        paddingVertical: 20,
        paddingHorizontal: 19,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25
    },
    headercard: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    seances: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
    },
    isParticipe: {
        height: 10,
        width: 10,
        borderRadius: 5,
        backgroundColor: 'green'
    },
    btnSegment: {
        height: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        marginTop: 20,
        marginHorizontal: 10,
    },
    textBtn: {
        color: '#fff',
        fontSize: 16
    },
    textInfo: {
        flex: 1,
        textAlignVertical: 'center',
        textAlign: 'center',
        color: '#777'
    },
    imageTheme: {
        height: 80,
        width: 80,
        borderRadius: 5,
        marginRight: 10
    },
    titleCulte: {
        fontSize: 18,
    },
    subTitleCulte: {
        color: '#777'
    },
    iconDirection: {
        height: 40,
        width: 40,
        borderRadius: 20,
        textAlign: 'center',
        textAlignVertical: 'center',
        backgroundColor: '#eee',
        color: PrimaryColor
    },
    contentIcon: {
        justifyContent: 'center'
    }
});