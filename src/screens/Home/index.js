import React, { PureComponent } from 'react';
import StackNavigation from '../../navigations/StackNavigation';
import { View, StatusBar } from 'react-native';
import { PrimaryColor } from '@styles'

class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  
  render() {
    return(
      <View style = {{flex: 1}}>
        <StatusBar backgroundColor = {PrimaryColor}/>
        <StackNavigation />
      </View>
    )
  }
}

export default Home;
