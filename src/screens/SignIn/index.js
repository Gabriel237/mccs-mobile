import React, { PureComponent } from 'react';
import {  View, Text, StatusBar, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { styles } from './style'
import FormGroup from '../../components/FormGroup';
import Loading from '../../components/Loading';
import { signIn } from '../../store/actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { store } from '../../store/configurestore'

class SignIn extends PureComponent {
  
  state = {
    isVisible: false
  }

  handleChange = (value, label) => {
      if(label == 'Username'){
        this.setState({email: value})
      }
      else if(label == 'Mot de passe') {
        this.setState({password: value})
      }
  }

  auth = () => {
    const { signIn, navigation } = this.props
    const data = {
      username: this.state.email,
      password: this.state.password
    }
    signIn(data, navigation)
  }

  render() {
    const { navigation, isLoading, messageError } = this.props
    return (
        <ImageBackground 
            source = {require('@assets/42644-m.jpg')} 
            style = {styles.container}
            imageStyle = {{top: -150}}
        >
        <View style = {styles.header}>
          <Image 
            source = {require('@assets/logo1.png')}
            style = {styles.logo}
          />
        </View>
        <ScrollView 
          style = {styles.content}
          contentContainerStyle = {{paddingBottom: 40}}
          keyboardShouldPersistTaps = 'always'
        >
            <Text style = {styles.title}>Connexion</Text>
            <Loading 
              isVisible = {isLoading}
            />
            <FormGroup 
              label = "Username"
              iconName = "email"
              onChangeText = {this.handleChange}
            />
            <FormGroup 
              label = "Mot de passe"
              iconName = "vpn-key"
              onChangeText = {this.handleChange}
            />
            {
              messageError && <Text style = {styles.messageError}>{messageError}</Text>
            }
            <View style = {styles.signUpBtn}>
                <Text style = {styles.signUpBtntitle}>Pas encore inscris?</Text>
                <TouchableOpacity onPress = {() => navigation.push('SignUp')}>
                    <Text style = {styles.signUpBtnText}>Inscrivez vous</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity style = {styles.btn} onPress = {() => this.auth()}>
              <Text style = {styles.textBtn}>Connexion</Text>
            </TouchableOpacity>
        </ScrollView>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.authReducer.isLoading,
    messageError: state.authReducer.messageError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({signIn}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
