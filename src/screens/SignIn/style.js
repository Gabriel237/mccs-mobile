import { StyleSheet } from 'react-native'
import { PrimaryColor } from '@styles'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: PrimaryColor
    },
    header: {
        height: 200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 100,
        height: 100,
        borderRadius: 10
    },
    content: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 20,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold'
    },
    btn: {
        height: 50,
        width: '100%',
        backgroundColor: PrimaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        marginTop: 20
    },
    textBtn: {
        color: '#fff',
        fontSize: 16
    },
    signUpBtn: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        marginTop: 20
    },
    signUpBtnText: {
        color: PrimaryColor
    },
    signUpBtntitle: {
        color: '#777'
    },
    messageError: {
        color: '#ff5252'
    }
});