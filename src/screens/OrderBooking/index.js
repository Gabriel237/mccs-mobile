import React, { PureComponent } from 'react';
import {  View, Text, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { styles } from './style'
import { Icon } from 'react-native-elements';
import AddorderUser from '../../components/AddOrderUser';
import { myhelp, signIn, createOrderUser, orderBooking } from '../../store/actions'
import Loading from '../../components/Loading';
import {SET_MESSAGE_ERROR, SET_MESSAGE_SUCCES } from '../../store/type'
import RequestError from '../../components/RequestError';
import RequestSucces from '../../components/RequestSucces';

class OrderBooking extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
        isVisibleModal: false,
    };
  }

  componentDidMount() {
      this.loadData()
  }

  toggleModal = () => {
    this.setState({isVisibleModal: !this.state.isVisibleModal})
  }

  toggleModalSucces = () => {
    const { dispatch } = this.props
    dispatch({type: SET_MESSAGE_SUCCES, value: ''})
  }

  toggleModalError = () => {
    const { dispatch } = this.props
    dispatch({type: SET_MESSAGE_ERROR, value: ''})
  }

  loadData = () => {
    const { userConnect, myhelp, memoryUser, signIn } = this.props
    signIn(memoryUser).then((res) => {
        if(res.access) {
            myhelp(userConnect.id, res.access)
        }
    })
  }

  orderBooking = (user, idfideles) => {
    const { route, createOrderUser, signIn, orderBooking, memoryUser, userConnect } = this.props
    const reservation = route.params.data
    reservation.fidele_aide = userConnect.id
    reservation.aide = true
    if(idfideles){
      reservation.fideles = idfideles
      signIn(memoryUser)
      .then((responseAuth) => {
        orderBooking(responseAuth.access, reservation)
        this.loadData()
      })
    }
    else{
      user.createdByFidele = userConnect.id
      user.aide = true
      createOrderUser(user)
      .then((res) => {
          this.toggleModal()
          reservation.fideles = res.id
          signIn(memoryUser)
          .then((responseAuth) => {
            orderBooking(responseAuth.access, reservation)
            this.loadData()
          })
      })
    }
  }

  renderItem = ({item}) => {
    return(
        <View style = {styles.card}>
          <View style = {{flex: 1}}>
            <Text style = {styles.titleCard}>{item.first_name} {item.last_name}</Text>
            <Text style = {styles.subTitle}>{item.phone}</Text>
          </View>
          <TouchableOpacity
            style = {styles.btn}
            onPress = {() => this.orderBooking(null,item.id)} 
          >
            <Text style = {styles.textBtn}>Réserver</Text>
          </TouchableOpacity>
        </View>
    )
  }

  render() {
    const { mesaides, route } = this.props
    const reservation = route.params.data
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>
          <Text style = {styles.headerTitle}>Pour qui souhaitez-vous réserver?</Text>
        </View>
        <View style = {styles.content}>
          <AddorderUser 
            isVisible = {this.state.isVisibleModal}
            toggleModal = {this.toggleModal}
            orderBooking = {this.orderBooking}
          />
          <Loading
            isVisible = {this.props.isLoading}
          />
          {
            this.props.messageSucces != ''?
            <RequestSucces
                toggleModal = {this.toggleModalSucces}
                messageSucces = {this.props.messageSucces}
            />:null
          }
          {
            this.props.messageError != ''?
            <RequestError
                toggleModal = {this.toggleModalError}
                messageError = {this.props.messageError}
            />:null
          }
          <FlatList
            data = {mesaides}
            renderItem = {this.renderItem}
          />
          <TouchableOpacity 
            style = {styles.btnFloating}
            onPress = {() => this.toggleModal()}
          >
            <Icon
                name = 'add'
                type = 'material'
                color = '#fff'
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      isLoading: state.authReducer.isLoading,
      userConnect: state.authReducer.user,
      cultes: state.culteReducer.list,
      messageError: state.bookingReducer.messageError,
      messageSucces: state.bookingReducer.messageSucces,
      mesaides: state.bookingReducer.myhelp,
      memoryUser: state.authReducer.memoryUser,
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        ...bindActionCreators({myhelp, signIn, createOrderUser, orderBooking}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderBooking);
