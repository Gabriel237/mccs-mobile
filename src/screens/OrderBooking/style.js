import { StyleSheet, Dimensions } from 'react-native'
import { PrimaryColor } from '@styles'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: PrimaryColor
    },
    header: {
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    headerTitle: {
        fontSize: 22,
        color: '#fff'
    },
    content: {
        flex: 1,
        backgroundColor: '#fff',
        paddingVertical: 20,
        paddingHorizontal: 19,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25
    },
    btnFloating: {
        width: 56,
        height: 56,
        borderRadius: 28,
        backgroundColor: PrimaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 20,
        right: 20
    },
    btn: {
        width: 100,
        height: 30,
        borderRadius: 15,
        backgroundColor: PrimaryColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textBtn: {
        color: '#fff'
    },
    card: {
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: '#000',
        flexDirection: 'row',
        shadowOffset: {height: 1, width: 1},
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 3,
        marginHorizontal: 2,
        marginVertical: 10,
        padding: 10
    },
    titleCard: {
        fontSize: 18
    },
    subTitle: {
        color: '#777'
    }
})