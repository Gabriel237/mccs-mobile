import React, { PureComponent } from 'react';
import {  View, Text, FlatList, ImageBackground, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { styles } from './style'
import { Icon } from 'react-native-elements'
import { mybooking, signIn, deletebooking, listOrderBooking } from '../../store/actions'
import { capitalize } from '../../utils/capitalize'
import Loading from '../../components/Loading';
import ConfirmDelete from '../../components/ConfirmDelete';

class MesReservation extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      is_myBooking: true
    };
  }

  componentDidMount() {
    this.loadData()
    this.loadOrderBooking()
    const { navigation } = this.props;
    navigation.addListener("focus", () => {
      this.loadData()
      this.loadOrderBooking()
    });
  }

  loadOrderBooking() {
    const { signIn, memoryUser, listOrderBooking } = this.props
    signIn(memoryUser).then((res) => {
      if(res.access){
        listOrderBooking(res.id, res.access)
      }
    })
  }

  loadData() {
    const { mybooking, signIn, memoryUser, mybookings } = this.props
    signIn(memoryUser).then((res) => {
      if(res.access){
        mybooking(res.id, res.access)
        if(mybookings !== null && mybookings.length == 0){
          this.setState({
            MessageInfo: 'Vous n\'avez réservé aucune seance pour le moment'
          })
        }
      }
    })
  }

  toggleModal = (item) => {
    if(item) {
      this.setState({
        isVisible: !this.state.isVisible,
        idReservation: item.id
      })
    }
    else{
      this.setState({
        isVisible: !this.state.isVisible,
      })
    }
  }

  deleteReservation = () => {
    const { signIn, memoryUser, deletebooking } = this.props
    this.toggleModal()
    signIn(memoryUser).then((res) => {
      if(res.access){
        deletebooking(this.state.idReservation, res.access)
      }
    })
  }

  renderMyBookItem = ({item, index}) => {
    return(
      <TouchableOpacity 
        onPress = {() => this.props.navigation.navigate('Reservation', {culte: item})}
        style = {styles.card}>
         <View style = {{flex:1}}>
          <Text style = {{fontSize: 18}}>{capitalize(item.cultes.nomCulte)}</Text>
          <Text style = {{color: '#777'}}>{capitalize(item.seances.nomSeance)}</Text>
         </View>
          <TouchableOpacity 
            onPress = {() => this.toggleModal(item)}
            style = {styles.contentIcon}>
              <Icon
                name = "delete"
                type = "material"
                size = {25}
                iconStyle = {styles.iconDirection}
              />
          </TouchableOpacity>
      </TouchableOpacity>
    )
  }

  renderItem = ({item}) => {
    return(
      <View style = {styles.card}>
        <View style = {{flex:1}}>
          <Text style = {{fontSize: 18}}>{capitalize(item.fideles.username)}</Text>
          <Text style = {{fontSize: 18}}>{capitalize(item.cultes.nomCulte)}</Text>
          <Text style = {{color: '#777'}}>{capitalize(item.seances.nomSeance)}</Text>
        </View>
      </View>
    )
  }

  render() {
    const { mybookings, isLoading, list_orderBooking } = this.props
    console.log('list_orderBooking', list_orderBooking)
    return (
        <ImageBackground 
          source = {require('@assets/42644-m.jpg')}
          style = {styles.container}
          imageStyle = {{top: -250}}
        >
            <View style = {styles.header}>
                <Text style = {styles.headerTitle}>Mes Réservations</Text>
            </View>
            <View style = {styles.content}>
              <View style = {[styles.card,{marginTop: -50}]}>
                  <TouchableOpacity
                     style = {styles.btn}
                     onPress = {() => this.setState({is_myBooking: true})}
                  >
                    <Text style = {styles.textBtn}>Mes Réservations</Text>
                  </TouchableOpacity>
                  <TouchableOpacity 
                    style = {styles.btn}
                    onPress = {() => this.setState({is_myBooking: false})}
                  >
                    <Text style = {styles.textBtn}>Autres</Text>
                  </TouchableOpacity>
              </View>
                <Loading 
                  isVisible = {isLoading}
                />
                <ConfirmDelete 
                  isVisible = {this.state.isVisible}
                  toggleModal = {this.toggleModal}
                  deleteReservation = {this.deleteReservation}
                />
                {
                  this.state.is_myBooking && mybookings !== null && mybookings.length != 0?
                  <FlatList 
                    data = {mybookings}
                    refreshing = {false}
                    onRefresh = {() => this.loadData()}
                    renderItem = {this.renderMyBookItem}
                  />:
                  !this.state.is_myBooking?
                  <FlatList 
                    data = {list_orderBooking}
                    refreshing = {false}
                    showsVerticalScrollIndicator = {false}
                    renderItem = {this.renderItem}
                  />:
                  <Text style = {styles.textInfo}>{this.state.MessageInfo}</Text>
                }
            </View>
        </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      isLoading: state.authReducer.isLoading,
      user: state.authReducer.user,
      mybookings: state.bookingReducer.mybooking,
      list_orderBooking: state.bookingReducer.listOrderBooking,
      memoryUser: state.authReducer.memoryUser,
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({mybooking, signIn, deletebooking, listOrderBooking}, dispatch)
  }

export default connect(mapStateToProps, mapDispatchToProps)(MesReservation);
