import { StyleSheet } from 'react-native'
import { PrimaryColor } from '@styles'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: PrimaryColor
    },
    header: {
        height: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 22,
        color: '#fff'
    },
    card: {
        backgroundColor: '#fff',
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {height: 1, width: 1},
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 3,
        marginHorizontal: 2,
        marginVertical: 10,
        padding: 10
    },
    content: {
        flex: 1,
        backgroundColor: '#fff',
        paddingVertical: 20,
        paddingHorizontal: 19,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25
    },
    btn: {
        height: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: PrimaryColor,
        borderRadius: 25,
        marginHorizontal: 10
    },
    textBtn: {
        color: '#fff'
    },
    fab: {
        position: 'absolute',
        backgroundColor: PrimaryColor,
        margin: 16,
        right: 0,
        bottom: 0,
    },
    textInfo: {
        flex: 1,
        textAlignVertical: 'center',
        textAlign: 'center',
        color: '#777'
    },
    iconDirection: {
        height: 40,
        width: 40,
        borderRadius: 20,
        textAlign: 'center',
        textAlignVertical: 'center',
        backgroundColor: '#eee',
        color: '#fab1a0'
    },
    contentIcon: {
        justifyContent: 'center'
    }
});