import { StyleSheet, Dimensions } from 'react-native'
import { PrimaryColor } from '@styles'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: PrimaryColor
    },
    header: {
        height: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 22,
        color: '#fff'
    },
    contentRadionButton: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    card: {
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {height: 1, width: 1},
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 3,
        marginHorizontal: 2,
        marginVertical: 10,
        padding: 10
    },
    titleCard: {
        fontSize: 18,
        color: '#777'
    },
    content: {
        flex: 1,
        backgroundColor: '#fff',
        paddingVertical: 20,
        paddingHorizontal: 19,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25
    },
    btn: {
        height: 50,
        width: '100%',
        backgroundColor: PrimaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        marginTop: 20,
    },
    textBtn: {
        color: '#fff',
        fontSize: 16
    },
    input: {
        height: 50,
        width: '100%',
        borderRadius: 5,
        paddingLeft: 10,
        borderColor: '#f3f3f3',
        borderWidth: 1
    },
    addUser: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginTop: 20,
        color: '#fff',
        backgroundColor: PrimaryColor,
        textAlign: 'center',
        textAlignVertical: 'center',
        right: '-40%'
    }
});