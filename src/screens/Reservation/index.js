import React, { PureComponent } from 'react';
import {  View, Text, ScrollView, TouchableOpacity, TextInput, ImageBackground,  } from 'react-native';
import RadioButton from 'react-native-paper/lib/commonjs/components/RadioButton/RadioButton';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { NavigationActions, StackActions } from 'react-navigation';
import { Icon } from 'react-native-elements'

import { styles } from './style'
import { booking, signIn, updatebooking } from '../../store/actions'
import RequestError from '../../components/RequestError';
import { SET_MESSAGE_ERROR, SET_MESSAGE_SUCCES } from '../../store/type'
import Loading from '../../components/Loading';
import RequestSucces from '../../components/RequestSucces';

class Reservation extends PureComponent {
    
  constructor(props) {
    super(props);
    this.state = {
        is_familly: false,
        is_order: false,
        seance: '',
        isVisible: false,
        nbplace: 1
    };
  }

  componentDidMount() {
    this.props.navigation.addListener('focus', () => {
        const seletedSeance = this.props.route.params.culte.seances?this.props.route.params.culte.seances.id:''
        const { culte } = this.props.route.params
        this.setState({
            seance: seletedSeance,
            is_participe: culte.cultes?true:'',
            nbplace: culte.nbplace?culte.nbplace:1,
            is_familly: culte.nbplace && culte.nbplace!=1?true: false
        })
    })
    
  }

  toggleModal = () => {
    const { dispatch } = this.props
    dispatch({type: SET_MESSAGE_ERROR, value: ''})

  }

  toggleModalSucces = () => {
    const { dispatch, navigation } = this.props
    dispatch({type: SET_MESSAGE_SUCCES, value: ''})
    Promise.all([
        navigation.dispatch(
            StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'TabNavigation' })]
          })
        )
      ]).then(() => navigation.navigate('MesReservation'))
  }

  booking = () => {
    const { route, user, booking, navigation, signIn, memoryUser } = this.props
    const idculte = route.params.culte.id
    const data = {
        cultes: idculte,
        seances: this.state.seance,
        fideles: user.id,
        nbplace: parseInt(this.state.nbplace)
    }
    if(this.state.is_participe && this.state.is_order){
        navigation.navigate('OrderBooking', {data: data})
    }else{
        signIn(memoryUser).then((res) => {
            if(res.access) {
                booking(res.access, data, navigation)
            }
        })
    }
  }

  updateBooking = () => {
    const { route, user, updatebooking, navigation, signIn, memoryUser } = this.props
    const idculte = route.params.culte.cultes.id
    const nbplace = route.params.culte.nbplace
    const idReservation = route.params.culte.id
    const data = {
        id: idReservation,
        cultes: idculte,
        seances: this.state.seance,
        fideles: user.id,
        nbplace: this.state.nbplace != 1?parseInt(this.state.nbplace):nbplace
    }
    console.log('data', data)
    signIn(memoryUser).then((res) => {
        if(res.access) {
            updatebooking(idReservation,res.access, data)
        }
    })
  }

  render() {
    const { navigation } = this.props
    const { culte } = this.props.route.params
    const {seances} = culte.cultes?culte.cultes:culte
    return (
        <ImageBackground 
            source = {require('@assets/42644-m.jpg')}
            style = {styles.container}
            imageStyle = {{top: -250}}
        >
            <View style = {styles.header}>
                <Text style = {styles.headerTitle}>{culte.cultes?culte.cultes.nomCulte:culte.nomCulte}</Text>
            </View>
            <ScrollView 
                style = {styles.content}
                contentContainerStyle = {{paddingBottom: 40}}
                keyboardShouldPersistTaps = 'always'
            >
                <Loading 
                    isVisible = {this.props.isLoading}
                />
            {
                this.props.messageSucces != ''?
                <RequestSucces
                    toggleModal = {this.toggleModalSucces}
                    messageSucces = {this.props.messageSucces}
                />:null
            }
            {
                this.props.messageError != ''?
                <RequestError
                    toggleModal = {this.toggleModal}
                    messageError = {this.props.messageError}
                />:null
            }
            <View style = {styles.card}>
                <Text style = {styles.titleCard}>Souhaitez-vous participer à ce culte ?</Text>
                    <View style = {styles.contentRadionButton}>
                        <RadioButton
                            value= {true}
                            status={ this.state.is_participe === true ? 'checked' : 'unchecked' }
                            onPress={() => this.setState({is_participe: true})}
                        />
                        <Text>Oui</Text>
                    </View>
                    <View style = {styles.contentRadionButton}>
                        <RadioButton
                            value={false}
                            status={ this.state.is_participe === false ? 'checked' : 'unchecked' }
                            onPress={() => this.setState({is_participe: false})}
                        />
                        <Text>Non</Text>
                    </View>
            </View>
            <View style = {styles.card}>
                <Text style = {styles.titleCard}>Sélectionnez la séance de votre choix ?</Text>
                {
                    seances.map((item) => {
                        return(
                        <View style = {styles.contentRadionButton}>
                            <RadioButton
                                value={item.id}
                                status={ this.state.seance === item.id ? 'checked' : 'unchecked' }
                                onPress={() => this.setState({seance: item.id})}
                            />
                            <Text>{item.nomSeance}</Text>
                        </View>
                        )
                    }) 
                }
            </View>
            <View style = {styles.card}>
                <Text style = {styles.titleCard}>Viendrez-vous accompagné (e) / en famille ?</Text>
                    <View style = {styles.contentRadionButton}>
                        <RadioButton
                            value={true}
                            status={ this.state.is_familly == true ? 'checked' : 'unchecked' }
                            onPress={() => this.setState({is_familly: true})}
                        />
                        <Text>Oui</Text>
                    </View>
                    <View style = {styles.contentRadionButton}>
                        <RadioButton
                            value={false}
                            status={ this.state.is_familly == false ? 'checked' : 'unchecked' }
                            onPress={() => this.setState({is_familly: false})}
                        />
                        <Text>Non</Text>
                    </View>
            </View>
            <View style = {styles.card}>
                <Text style = {styles.titleCard}>Pour qui réservez-vous ?</Text>
                    <View style = {styles.contentRadionButton}>
                        <RadioButton
                            value={false}
                            status={ this.state.is_order == false ? 'checked' : 'unchecked' }
                            onPress={() => this.setState({is_order: false})}
                        />
                        <Text>Vous</Text>
                    </View>
                    <View style = {styles.contentRadionButton}>
                        <RadioButton
                            value={true}
                            status={ this.state.is_order == true ? 'checked' : 'unchecked' }
                            onPress={() => this.setState({is_order: true})}
                        />
                        <Text>Autres</Text>
                    </View>
            </View>
            {
                this.state.is_familly && 
                <View>
                    <Text style = {styles.titleCard}>Combien serez-vous?</Text>
                    <TextInput 
                        style = {styles.input}
                        defaultValue = {`${this.state.nbplace}`}
                        keyboardType = 'numeric'
                        onChangeText = {(value) => this.setState({nbplace: value})}
                    />
                </View>
            }
            {
                this.state.is_participe && !this.state.is_order && !culte.cultes ? 
                <TouchableOpacity  
                    style = {styles.btn} 
                    onPress = {() => this.booking()}>
                    <Text style = {styles.textBtn}>Valider</Text>
                </TouchableOpacity>:
                this.state.is_participe && this.state.is_order && !culte.cultes ? 
                <TouchableOpacity  
                    style = {styles.btn} 
                    onPress = {() => this.booking()}
                >
                    <Text style = {styles.textBtn}>Suivant</Text>
                </TouchableOpacity>
                :culte.cultes?
                <TouchableOpacity  
                    style = {styles.btn} 
                    onPress = {() => this.updateBooking()}>
                    <Text style = {styles.textBtn}>Modifier</Text>
                </TouchableOpacity>:null
            }
        </ScrollView>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      isLoading: state.authReducer.isLoading,
      user: state.authReducer.user,
      cultes: state.culteReducer.list,
      messageError: state.bookingReducer.messageError,
      messageSucces: state.bookingReducer.messageSucces,
      memoryUser: state.authReducer.memoryUser,
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        ...bindActionCreators({booking, signIn, updatebooking}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Reservation);
