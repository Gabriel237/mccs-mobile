import { UPDATE_LIST_CULTE, SET_LOADING } from '../type'
const initialState  = {list: null, isLoading: false, }

const culteReducer = (state= initialState, action) => {
    let nextState;
    switch (action.type) {
        case UPDATE_LIST_CULTE:
            nextState = {
                ...state,
                list: action.value
            }
            return nextState;
        case SET_LOADING:
            nextState = {
                ...state,
                isLoading: action.value
            }
            return nextState;
        default:
            return state;
    }
}

export default culteReducer;