import { SET_MESSAGE_ERROR, UPDATE_MY_BOOKING, SET_MESSAGE_SUCCES, UPDATE_MY_HELP, UPDATE_LIST_ORDER_BOOKING } from '../type'
const initialState  = {messageError: '', mybooking: null, messageSucces: '', myhelp: [], listOrderBooking: []}

const bookingReducer = (state= initialState, action) => {
    let nextState;
    switch (action.type) {
        case SET_MESSAGE_ERROR:
            nextState = {
                ...state,
                messageError: action.value
            }
            return nextState;
        case SET_MESSAGE_SUCCES:
            nextState = {
                ...state,
                messageSucces: action.value
            }
            return nextState;
        case UPDATE_MY_BOOKING:
            nextState = {
                ...state,
                mybooking: action.value
            }
            return nextState;
        case UPDATE_MY_HELP:
            nextState = {
                ...state,
                myhelp: action.value
            }
            return nextState;
        case UPDATE_LIST_ORDER_BOOKING:
            nextState = {
                ...state,
                listOrderBooking: action.value
            }
            return nextState;
        default:
            return state;
    }
}

export default bookingReducer;