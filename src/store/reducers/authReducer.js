import { AUTH_USER, SET_LOADING } from '../type'
const initialState  = {user: {}, isLoading: false, messageError: null, memoryUser: {}}

const authReducer = (state= initialState, action) => {
    let nextState;
    switch (action.type) {
        case AUTH_USER:
            nextState = {
                ...state,
                user: action.value.user,
                messageError: action.value.messageError,
                memoryUser: action.value.memoryUser?action.value.memoryUser:{}
            }
            return nextState;
        case SET_LOADING:
            nextState = {
                ...state,
                isLoading: action.value
            }
            return nextState;
        default:
            return state;
    }
}

export default authReducer;