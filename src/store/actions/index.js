import { 
        auth, 
        getAllCulte, 
        reservation, 
        signUp, 
        mesreservation, 
        updateReservation, 
        deleteReservation, 
        mesaides,
        autreReservation, 
        listAutreReservation} from '../../api'
import { 
    SET_LOADING, 
    AUTH_USER, 
    UPDATE_LIST_CULTE, 
    SET_MESSAGE_ERROR, 
    UPDATE_MY_BOOKING, 
    SET_MESSAGE_SUCCES,
    UPDATE_MY_HELP,
    UPDATE_LIST_ORDER_BOOKING } from '../type'
import { store } from '../configurestore'

export const signIn = (data, navigation) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
   return new Promise((resolve) => {
    auth(data)
    .then((res)=> res.json())
    .then((res) => {
        dispath({type: SET_LOADING, value: false})
        if(res.access){
            dispath({
                type: AUTH_USER, 
                value: {
                    user: res,
                    messageError: null,
                    memoryUser: data
                }
            })
            if(navigation){
                navigation.push('TabNavigation')
            }
            resolve(res)
        }
        if(res.detail){
            dispath({
                type: AUTH_USER,
                value: {
                    user: {},
                    messageError: 'username ou mot de passe incorrect'
                }
            })
        }
        if(res.non_field_errors){
            dispath({
                type: AUTH_USER,
                value: {
                    user: {},
                    messageError: 'username ou mot de passe incorrect'
                }
            })
        }
    })
    .catch((err) => {
        dispath({type: SET_LOADING, value: false})
    })
   })
}

export const listCulte = (token) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
    getAllCulte(token)
    .then((res) => res.json())
    .then((res) => {
        dispath({type: SET_LOADING, value: false})
        if(Array.isArray(res)){
            dispath({type: UPDATE_LIST_CULTE, value: res})
        }
        else{
            let data = store.getState().authReducer.memoryUser
            dispath(signIn(data))
        }
    })
    .catch((err) => {
        dispath({type: SET_LOADING, value: false})
    })
}

export const booking = (token, data) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
    reservation(token, data)
    .then((res) => res.json())
    .then((res) => {
        dispath({type: SET_LOADING, value: false})
        if(res.error){
            dispath({type: SET_MESSAGE_ERROR, value: res.error[0]})
        }
        else if (res.code) {
            let data = store.getState().authReducer.memoryUser
            dispath(signIn(data))
        }
        else if(res.dateCreated) {
            dispath({type: SET_MESSAGE_SUCCES, value: 'Réservation effectué avec succès'})
        }
        console.log('res bookin', res)
    })
    .catch((err) => {
        dispath({type: SET_LOADING, value: false})
        console.log('err', err)
    })
}

export const register = (data, navigation) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
    signUp(data)
    .then((res) => res.json())
    .then((res) => {
        dispath({type: SET_LOADING, value: false})
        if(res.is_abonne) {
            let user = {username: data.username, password: data.password}
            dispath(signIn(user))
            navigation.push('TabNavigation')
        }
        if(res.email && Array.isArray(res.email)) {
            dispath({type: SET_MESSAGE_ERROR, value: "Adresse email invalid"})
        }
        if(res.username && Array.isArray(res.username)) {
            dispath({type: SET_MESSAGE_ERROR, value: res.username[0]})
        }
    })
    .catch((err) => {
        dispath({type: SET_LOADING, value: false})
    })
}

export const mybooking = (idUser, token) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
    mesreservation(idUser, token)
    .then((res) => res.json())
    .then((res) => {
        dispath({type: SET_LOADING, value: false})
        if(Array.isArray(res)){
            dispath({type: UPDATE_MY_BOOKING, value: res})
        }
        else{
            let user = {username: data.username, password: data.password}
            dispath(signIn(user))
        }
    })
    .catch((err) => {
        dispath({type: SET_LOADING, value: false})
    })
}

export const updatebooking = (idReservation, token, data) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
    updateReservation(idReservation, token, data)
    .then((res) => res.json())
    .then((res) => {
        dispath({type: SET_LOADING, value: false})
        if(res.error){
            dispath({type: SET_MESSAGE_ERROR, value: res.error[0]})
        }
        else if(res.dateCreated) {
            dispath({type: SET_MESSAGE_SUCCES, value: 'Modification effectué avec succès'})
        }
        console.log('resp update', res)
    })
    .catch((err) => {
        dispath({type: SET_LOADING, value: false})
        console.log('err update', err)
    })
}

export const deletebooking = (idReservation, token) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
    deleteReservation(idReservation, token)
    .then((res) => {
        dispath({type: SET_LOADING, value: false})
        if(res.status == 200) {
            const mybooking = store.getState().bookingReducer.mybooking
           const newbooking =  mybooking.filter((item) => item.id != idReservation)
           dispath({type: UPDATE_MY_BOOKING, value: newbooking})
            
        }
    })
    .catch((err) => {
        dispath({type: SET_LOADING, value: false})
    })
}

export const myhelp = (idUser, token) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
    mesaides(idUser, token)
    .then((res) => res.json())
    .then((res) => {
        dispath({type: SET_LOADING, value: false})
        dispath({type: UPDATE_MY_HELP, value: res})
        console.log('res myhelp', res)
    })
    .catch((err) => {
        dispath({type: SET_LOADING, value: false})
        console.log('err myhelp', err)
    })
}

export const createOrderUser = (data) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
    return new Promise((resolve, reject) => {
        signUp(data)
        .then((res) => res.json())
        .then((res) => {
            dispath({type: SET_LOADING, value: false})
            if(res.is_abonne) {
                resolve(res)
            }
            if(res.email && Array.isArray(res.email)) {
                dispath({type: SET_MESSAGE_ERROR, value: "Adresse email invalid"})
            }
            if(res.username && Array.isArray(res.username)) {
                dispath({type: SET_MESSAGE_ERROR, value: "vous n'avez renseigné le username"})
            }
            if(res.password && Array.isArray(res.password)) {
                dispath({type: SET_MESSAGE_ERROR, value: "vous n'avez renseigné le mot de passe"})
            }
            console.log('create order user', res)
        })
        .catch((err) => {
            dispath({type: SET_LOADING, value: false})
            reject(err)
            console.log('err order user', err)
        })
    })
}

export const orderBooking = (token, data) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
    autreReservation(token, data)
    .then((res) => res.json())
    .then((res) => {
        dispath({type: SET_LOADING, value: false})
        if(res.error){
            dispath({type: SET_MESSAGE_ERROR, value: res.error[0]})
        }
        else if(res.dateCreated) {
            dispath({type: SET_MESSAGE_SUCCES, value: 'Réservation effectué avec succès'})
        }
        console.log('res order booking', res)
    })
    .catch((err) => {
        dispath({type: SET_LOADING, value: false})
        console.log('err', err)
    })
}

export const listOrderBooking = (idFidele, token) => (dispath) => {
    dispath({type: SET_LOADING, value: true})
    listAutreReservation(idFidele, token)
    .then((res) => res.json())
    .then((res) => {
        dispath({type: SET_LOADING, value: false})
        if(Array.isArray(res)){
            dispath({type: UPDATE_LIST_ORDER_BOOKING, value: res})
        }
        console.log('list order booking', res)
    })
    .catch((err) => {
        dispath({type: SET_LOADING, value: false})
        console.log('err list orderbooking', err)
    })
}


