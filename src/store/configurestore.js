import { createStore, combineReducers, applyMiddleware } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-community/async-storage';
import authReducer from './reducers/authReducer'
import culteReducer from './reducers/culteReducer'
import bookingReducer from './reducers/bookingReducer'

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['bookingReducer', 'culteReducer']
}

const middleware = [thunk];

const rootReducer = combineReducers({
  authReducer,
  culteReducer,
  bookingReducer
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

let store = createStore(persistedReducer, applyMiddleware(...middleware))
let persistor = persistStore(store)

export { store, persistor }