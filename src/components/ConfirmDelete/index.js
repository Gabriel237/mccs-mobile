import React, { PureComponent } from 'react';
import {  View, Text, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal'
import { Icon } from 'react-native-elements'
import { styles } from './style'

class ConfirmDelete extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
      const { isVisible, toggleModal, deleteReservation } = this.props
    return (
      <View>
        <Modal 
          animationIn = 'zoomIn'
          isVisible = {isVisible}
          onBackButtonPress = {toggleModal}
          onBackdropPress = {toggleModal}
          style = {styles.container}
        >
          <View style = {styles.content}>
            <Text style = {styles.title}>Annuler la réservation</Text>
            <Text style = {styles.textError}>Souhaitez-vous vraiment annuler la réservation de ce culte?</Text>
            <View style = {styles.contentBtn}>
                <TouchableOpacity style = {[styles.btn,{backgroundColor: '#fab1a0',}]} onPress = {() => toggleModal()}>
                    <Text style = {styles.textBtn}>Annuler</Text>
                </TouchableOpacity>
                <TouchableOpacity style = {[styles.btn,{backgroundColor: '#2bcbba',}]} onPress = {() => deleteReservation()}>
                    <Text style = {styles.textBtn}>Valider</Text>
                </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default ConfirmDelete;
