import React, { PureComponent } from 'react';
import {  View, Text } from 'react-native';
import Spinner from 'react-native-spinkit';
import { styles } from './style';

class Loading extends PureComponent {
  
  render() {
      const { isVisible } = this.props
    return (
      <View style = {styles.container}>
        <Spinner 
          style={styles.spinner} 
          isVisible={isVisible} 
          size={40} 
          type='FadingCircleAlt' 
          color='#777'
        />
      </View>
    );
  }
}

export default Loading;
