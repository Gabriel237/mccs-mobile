import { StyleSheet } from 'react-native'
import { PrimaryColor } from '@styles'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 0,
    },
    content: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 20
    },
    header: {
        height: 56,
        borderBottomColor: '#f3f3f3',
        borderBottomWidth: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        justifyContent: 'center'
    },
    btn: {
        height: 50,
        width: '100%',
        backgroundColor: PrimaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        marginTop: 20
    },
    textBtn: {
        color: '#fff',
        fontSize: 16
    },
    btnClose: {
        height: 40,
        width: 40,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleContent: {
        fontSize: 18
    }
});