import React, { PureComponent } from 'react';
import {  View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { styles } from './style'
import Modal from 'react-native-modal'
import FormGroup from '../FormGroup';
import { Icon } from 'react-native-elements';
import { PrimaryColor } from '@styles'

class AddorderUser extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  handleChange = (value, label) => {
    if(label == 'Nom') {
      this.setState({first_name: value})
    }
    else if(label == 'Prenom') {
      this.setState({last_name: value})
    }
    else if(label == 'Telephone') {
      this.setState({phone: value})
    }
    else if(label == 'Mot de passe') {
      this.setState({password: value})
    }
    else if(label == 'Username') {
      this.setState({username: value})
    }
  }

  reserver = () => {
    const { orderBooking } = this.props
    const {first_name, last_name, password, phone, username } = this.state
    const user = {first_name, last_name, password, phone, username}
    orderBooking(user)
  }

  render() {
      const { isVisible, toggleModal } = this.props
    return (
      <View>
        <Modal
            isVisible = {isVisible}
            style = {styles.container}
        >
            <View style = {styles.header}>
                <TouchableOpacity 
                    style = {styles.btnClose}
                    onPress = {toggleModal}
                >
                    <Icon 
                        name = 'close'
                        type = 'material'
                        color = {PrimaryColor}
                    />
                </TouchableOpacity>
            </View>
            <ScrollView style = {styles.content}>
                <Text style = {styles.titleContent}>Vous allez réserver pour :</Text>
                <FormGroup 
                    label = "Nom"
                    iconName = "person"
                    onChangeText = {this.handleChange}
                />
                <FormGroup 
                    label = "Prenom"
                    iconName = "person"
                    onChangeText = {this.handleChange}
                />
                <FormGroup 
                    label = "Username"
                    iconName = "person"
                    onChangeText = {this.handleChange}
                />
                <FormGroup 
                    label = "Telephone"
                    iconName = "phone"
                    onChangeText = {this.handleChange}
                />
                <FormGroup 
                    label = "Mot de passe"
                    iconName = "vpn-key"
                    onChangeText = {this.handleChange}
                />
                <TouchableOpacity 
                    style = {styles.btn} 
                    onPress = {() => this.reserver()}
                >
                    <Text style = {styles.textBtn}>Réserver</Text>
                </TouchableOpacity>
            </ScrollView>
        </Modal>
      </View>
    );
  }
}

export default AddorderUser;
