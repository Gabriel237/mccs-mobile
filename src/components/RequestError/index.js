import React, { PureComponent } from 'react';
import {  View, Text } from 'react-native';
import Modal from 'react-native-modal'
import { Icon } from 'react-native-elements'
import { styles } from './style'

class RequestError extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { messageError, toggleModal } = this.props
    return (
      <View>
        <Modal 
          animationIn = 'zoomIn'
          isVisible = {true}
          onBackdropPress = {() => toggleModal()}
          onBackButtonPress = {() => toggleModal()}
          style = {styles.container}
        >
          <View style = {styles.content}>
              <Icon
                name = 'error'
                type = 'material'
                color = "#ff7979"
                size = {50}
              />
              <Text style = {styles.textError}>{messageError}</Text>
          </View>
        </Modal>
      </View>
    );
  }
}

export default RequestError;
