import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 0,
        paddingHorizontal: 20
    },
    content: {
        height: 200,
        width: '100%',
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 20,
    },
    textError: {
        marginTop: 10,
        textAlign: 'center',
        fontSize: 18,
        color: '#777'
    }
});