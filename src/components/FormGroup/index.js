import React, { PureComponent } from 'react';
import {  View, Text,TextInput } from 'react-native';
import { Icon } from 'react-native-elements'

import { styles } from './style'

class FormGroup extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { label, iconName, onChangeText } = this.props
    return (
      <View style = {styles.container}>
        <View style = {styles.contentLabel}>
            <Icon 
                type = 'material'
                name = {iconName}
                iconStyle = {styles.iconStyle}
            />
        </View>
        <TextInput 
            style = {styles.input}
            placeholder = {label}
            onChangeText = {(value) => onChangeText(value, label)}
        />
      </View>
    );
  }
}

export default FormGroup;
