import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginTop: 20
    },
    contentLabel: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
        width: 100,
        position: 'absolute',
        top: 1
    },
    textLabel: {
        color: '#777',
        marginLeft: 10,
    },
    iconStyle: {
        height: 30,
        width: 30,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 18,
        borderRadius: 15,
        color: '#777',
    },
    input: {
        height: 50,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: '#f3f3f3',
        paddingLeft: 30
    }
});