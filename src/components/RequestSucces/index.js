import React, { PureComponent } from 'react';
import {  View, Text, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal'
import { Icon } from 'react-native-elements'
import { styles } from './style'

class RequestSucces extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { messageSucces, toggleModal } = this.props
    return (
      <View>
        <Modal 
          animationIn = 'zoomIn'
          isVisible = {true}
          style = {styles.container}
        >
          <View style = {styles.content}>
              <Icon
                name = 'check-circle'
                type = 'material'
                color = "#2bcbba"
                size = {50}
              />
              <Text style = {styles.textError}>{messageSucces}</Text>
              <TouchableOpacity style = {styles.btn} onPress = {() => toggleModal()}>
                <Text style = {styles.textBtn}>Ok</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

export default RequestSucces;
