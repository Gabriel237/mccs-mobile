import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 0,
        paddingHorizontal: 20
    },
    content: {
        height: 250,
        width: '100%',
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 20,
    },
    textError: {
        marginTop: 10,
        textAlign: 'center',
        fontSize: 18,
        color: '#777'
    },
    btn: {
        height: 50,
        width: 150,
        backgroundColor: '#2bcbba',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        marginTop: 50,
        alignSelf: 'center'
    },
    textBtn: {
        color: '#fff',
        fontSize: 16
    },
});