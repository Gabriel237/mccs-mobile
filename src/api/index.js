import axios from 'axios'
import { baseUrl, baseUrlAuth } from './config'

export function auth(data) {
    return fetch( baseUrlAuth + '/api_login/', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
}

export function getAllCulte(token) {
    return fetch( baseUrl + '/cultesprogramme/', {
        method: 'GET',
        headers: {
            "Authorization": 'Bearer ' + token
        }
    })
}

export function reservation(token, data) {
    return fetch( baseUrl + '/reservations/', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            "Authorization": 'Bearer ' + token
        },
        body: JSON.stringify(data)
    })
}

export function signUp(data) {
    return fetch( baseUrl + '/fideles/', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    })
}

export function mesreservation(iduser, token) {
    return fetch( baseUrl + `/mesreservations/${iduser}/`, {
        method: 'GET',
        headers: {
            "Authorization": 'Bearer ' + token,
        },
    })
}

export function updateReservation(idReservation, token, data) {
    return fetch( baseUrl + `/reservations/${idReservation}/`, {
        method: 'PUT',
        headers: {
            "Authorization": 'Bearer ' + token,
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    })
}

export function deleteReservation(idReservation, token) {
    return fetch( baseUrl + `/reservations/${idReservation}/`, {
        method: 'DELETE',
        headers: {
            "Authorization": 'Bearer ' + token,
            Accept: 'application/json',
        },
    })
}

export function mesaides(idUser, token) {
    return fetch( baseUrl + `/mesaides/${idUser}/`, {
        method: 'GET',
        headers: {
            "Authorization": 'Bearer ' + token,
            Accept: 'application/json',
        },
    })
}

export function autreReservation(token, data) {
    return fetch( baseUrl + '/reservations_aide/', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            "Authorization": 'Bearer ' + token
        },
        body: JSON.stringify(data)
    })
}

export function listAutreReservation(idFifele, token) {
    return fetch( baseUrl + `/mes_reservations_aide/${idFifele}/`, {
        method: 'GET',
        headers: {
            "Authorization": 'Bearer ' + token,
            Accept: 'application/json'
        }
    })
}