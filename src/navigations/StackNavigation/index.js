import * as React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { connect } from 'react-redux'
import Reservation from '../../screens/Reservation';
import SignIn from '../../screens/SignIn';
import TabNavigation from '../TabNavigation';
import SignUp from '../../screens/SignUp';
import Home from '../../screens/Home';

const Stack = createStackNavigator();

class StackNavigation extends React.PureComponent {
    state = {
        intialRoute: 'SignIn'
    }

    render() {
        const { user } = this.props
        this.state.intialRoute = user.access?'TabNavigation':'SignIn'
        console.log('initial route name', this.state.intialRoute)
        return (
            <Stack.Navigator initialRouteName = {`${this.state.intialRoute}`}>
                <Stack.Screen 
                    name="SignIn" 
                    component={SignIn} 
                    options = {{headerShown: false}}
                />
                <Stack.Screen 
                    name="SignUp" 
                    component={SignUp} 
                    options = {{headerShown: false}}
                />
                <Stack.Screen 
                    name="TabNavigation" 
                    component={TabNavigation} 
                    options = {{headerShown: false}}
                />
            </Stack.Navigator>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      user: state.authReducer.user,
    }
  }
  
export default connect(mapStateToProps)(StackNavigation);