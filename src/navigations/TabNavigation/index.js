import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import ListCulte  from '../../screens/ListCulte';
import MesReservation from '../../screens/MesReservation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { PrimaryColor } from '@styles'
import Reservation from '../../screens/Reservation';
import OrderBooking from '../../screens/OrderBooking';

const Stack = createStackNavigator();

const StackNavigation = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen 
                name="Cultes" 
                component={ListCulte} 
                options = {{headerShown: false}}
            />
            <Stack.Screen 
                name="Reservation" 
                component={Reservation} 
                options = {{
                    headerShown: false,
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
                }}
            />
            <Stack.Screen 
                name="OrderBooking" 
                component={OrderBooking} 
                options = {{
                    headerShown: false,
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
                }}
            />
        </Stack.Navigator>
    )
}

const Tab = createBottomTabNavigator();

export default function TabNavigation() {
  return (
      <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'StackNavigation') {
                iconName = 'md-home'
            } else if (route.name === 'MesReservation') {
                iconName = 'ios-list';
            }
            return <Ionicons name={iconName} size={size} color={color} />;
            },
        })}
        
        tabBarOptions = {{
            activeTintColor: PrimaryColor,
            style: {
                height: 56
            },
            labelStyle: {
                fontSize: 14,
                top: -5
            }
        }}
      >
        <Tab.Screen 
            name="StackNavigation" 
            component={StackNavigation} 
            options = {{title: 'Cultes'}}
        />
        <Tab.Screen 
            name="MesReservation" 
            component={MesReservation} 
            options = {{title: 'Mes Réservations'}}
        />
      </Tab.Navigator>
  );
}